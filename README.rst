==================================
Bluetooth device services database
==================================

Bluetooth devices may offer multiple services.
Some are standarized, but some are proprietary.

This repository contains a list of text files with
information about real-world bluetooth devices.



Collecting information
======================

- Pair the device with your Linux computer and note the MAC address,
  e.g. ``80:6A:B0:B4:6D:DB``
- Run ``sudo hcitool info MACADDRESS``
- Run ``bluetoothctl info MACADDRESS``
- Put this information into a text file

File format
===========
- Multiple text blocks, each separated with a single empty line
- First block: Manual device description
- Second block: ``hcitool info`` output
- Third block: ``bluetoothctl info`` output

File name: MAC address without ``:`` + space + device name + ``.txt``


License
=======
CC0 - No Rights Reserved
